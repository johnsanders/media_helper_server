const fs = require("fs");
const fileHelpers = require("./helpers/fileHelpers");
const dbHelpers = require("./helpers/dbHelpers");
const googleHelpers = require("./helpers/googleHelpers");
const mediaPath = "../media_files/";
const bucketName = "rock-flag-126512";
const creds = {
	projectId: 'rock-flag-126512',
	keyFilename: './creds/keyfile.json'
};

const run = async () => {
	try {
		const items = await dbHelpers.getTranscribeItems();
		if (items.length > 0) {
			const file = fileHelpers.getFilenameWithoutExtension(items[0].filename) + ".flac";
			const language = items[0].language || "en-US";
			await googleHelpers.uploadFile(mediaPath + file, bucketName, creds);
			const transcript = await googleHelpers.getTranscription(file, language, creds);
			await dbHelpers.saveItem({id:items[0].id, transcriptReady:true, status:"Ready"});
			await dbHelpers.addTranscript(items[0].id, transcript);
			// TODO: delete remote file
		}	
	} catch(err) {
		console.error(err);
		fileHelpers.setLockStatus("0");
	}
};

if (fileHelpers.getLockStatus()) {
	process.exit();
} else {
	fileHelpers.setLockStatus("1");
	run();
	fileHelpers.setLockStatus("0");
}
