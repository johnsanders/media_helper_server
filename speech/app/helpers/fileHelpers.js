const fs = require("fs");
const lockFile = "./app/config/renderLock.dat";

module.exports.getLockStatus = () => {
	const status = fs.readFileSync(lockFile, 'utf8');
	return status.substring(0,1) === "1";
};

module.exports.setLockStatus = (status) => {
	fs.writeFileSync(lockFile, status, 'utf8');
};

module.exports.getFilenameWithoutExtension = (filename) => {
	return filename.replace(".mxf", "");
};
