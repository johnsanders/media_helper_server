const axios = require("axios");
const cakeBase = "http://cnnitouch/cake3/cnnitouch/";
const itemsTable = "cake-mh-items";
const transcriptsTable = "cake-mh-transcripts/";
const dbUrl = "http://cnnitouch-prod1/apps/flashSQLinterface/read3.php?table=cake_mh_items&where=status='Awaiting Transcript'";

module.exports.getTranscribeItems = () => {
	return new Promise( (resolve, reject) => {
		axios.get(dbUrl)
			.then( response => resolve(response.data) )
			.catch( err => reject(err) );
	});
};

module.exports.saveItem = item => {
	return axios.post(cakeBase + itemsTable + "/edit/" + item.id + ".json", item);
};

module.exports.addTranscript = (itemId, transcript) => {
	const toAdd = Object.assign(transcript, {item_id:itemId});
	return axios.post(cakeBase + transcriptsTable + "add.json", toAdd);
};
