const Speech = require('@google-cloud/speech');
const Storage = require('@google-cloud/storage');

module.exports.getTranscription = (filename, language, creds) => {
	return new Promise( (resolve, reject) => {
		const speech = Speech(creds);
		const request = {
			config: {
				encoding:"FLAC",
				sampleRateHertz:48000,
				languageCode:language,
				enableWordTimeOffsets:true
			},
			audio: {
				uri:"gs://rock-flag-126512/" + filename
			}
		};
		speech.longRunningRecognize(request)
			.then( data => {
				const operation = data[0];
				return operation.promise();
			})
			.then(response => {
			const results = response[0].results;
			const transcript = results.map(result => result.alternatives[0].transcript).join(" ");
			const words = JSON.stringify(
				results.map(result => result.alternatives[0].words)
					.reduce( (acc, wordGroup) => acc.concat(wordGroup), [] )
			);
			const firstConfidence = results[0].alternatives[0].confidence;
			const confidence = results.map( result => result.alternatives[0].confidence )
				.reduce( (acc, confidence) => (confidence+acc)/2, firstConfidence);
			resolve( {
				transcript:transcript,
				words:words,
				confidence:confidence
			});
		})
			.catch(err => reject(err));
	});
};

module.exports.uploadFile = (localFile, bucketName, creds) => {
	return new Promise( (resolve, reject) => {
		const storage = Storage(creds);
		storage
			.bucket(bucketName)
			.upload(localFile)
			.then(() => {
				resolve();
			})
			.catch((err) => {
				reject(err);
			});
	});
};
