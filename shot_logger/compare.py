#http://www.guguncube.com/1656/python-image-similarity-comparison-using-several-techniques
import os, time, re
from PIL import Image


def main():
	base_directory = '/home/cnnitouch/video_analyzer/frames/'
	num_images = 812
	flash_frame_check = 3
	thresh_bands_low = 1200000
	thresh_bands_high = 1800000
	thresh_histo = 40
	thresh_hash = 1200

	compare(num_images, base_directory, thresh_bands_low, thresh_bands_high, thresh_histo, thresh_hash)

def compare(num_images, base_directory, thresh_bands_low, thresh_bands_high, thresh_histo, thresh_hash):
	idx=1 
	while idx < num_images:
		filepaths = [os.path.join(base_directory, "%03d" % (idx,) + ".png"), os.path.join(base_directory, "%03d" % (idx+1,)) + ".png"]
		idx+=1

		image_filepath1 = filepaths[0]
		image_filepath2 = filepaths[1]

		sim_bands = image_similarity_bands_via_numpy(filepaths[0], filepaths[1])
		sim_histo = image_similarity_histogram_via_pil(filepaths[0], filepaths[1])
		sim_hash = image_similarity_greyscale_hash_code(filepaths[0], filepaths[1])

		is_cut = False
		if ( sim_bands > thresh_bands_low and  sim_histo > thresh_histo and sim_hash > thresh_hash):
			is_cut = True
		elif ( sim_bands > thresh_bands_high ):
			is_cut = True

		if (is_cut):
			print( "%s %s %s %s" % (idx, sim_bands, round(sim_histo, 1), sim_hash) )


def image_similarity_bands_via_numpy(filepath1, filepath2):
	from PIL import Image
	import math
	import operator
	import numpy
	image1 = Image.open(filepath1)
	image2 = Image.open(filepath2)
	image1 = get_thumbnail(image1)
	image2 = get_thumbnail(image2)
	if image1.size != image2.size or image1.getbands() != image2.getbands():
		return -1
	s = 0
	for band_index, band in enumerate(image1.getbands()):
		m1 = numpy.array([p[band_index] for p in image1.getdata()]).reshape(*image1.size)
		m2 = numpy.array([p[band_index] for p in image2.getdata()]).reshape(*image2.size)
		s += numpy.sum(numpy.abs(m1-m2))
	return s

def image_similarity_histogram_via_pil(filepath1, filepath2):
	from PIL import Image
	import math
	import operator
	image1 = Image.open(filepath1)
	image2 = Image.open(filepath2)
	image1 = get_thumbnail(image1)
	image2 = get_thumbnail(image2)
	h1 = image1.histogram()
	h2 = image2.histogram()
	rms = math.sqrt(reduce(operator.add,  list(map(lambda a,b: (a-b)**2, h1, h2)))/len(h1) )
	return rms

def image_similarity_greyscale_hash_code(filepath1, filepath2):
    image1 = Image.open(filepath1)
    image2 = Image.open(filepath2)
    image1 = get_thumbnail(image1, greyscale=True)
    image2 = get_thumbnail(image2, greyscale=True)
    code1 = image_pixel_hash_code(image1)
    code2 = image_pixel_hash_code(image2)
    res = hamming_distance(code1,code2)
    return res

def image_pixel_hash_code(image):
    pixels = list(image.getdata())
    avg = sum(pixels) / len(pixels)
    bits = "".join(map(lambda pixel: '1' if pixel < avg else '0', pixels))  # '00010100...'
    hexadecimal = int(bits, 2).__format__('016x').upper()
    return hexadecimal

def hamming_distance(s1, s2):
    len1, len2= len(s1),len(s2)
    if len1!=len2:
        if len1>len2:
            s1=s1[:-(len1-len2)]
        else:
            s2=s2[:-(len2-len1)]
    assert len(s1) == len(s2)
    return sum([ch1 != ch2 for ch1, ch2 in zip(s1, s2)])

def get_thumbnail(image, size=(128,128), stretch_to_fit=False, greyscale=False):
    if not stretch_to_fit:
        image.thumbnail(size, Image.ANTIALIAS)
    else:
        image = image.resize(size)
    if greyscale:
        image = image.convert("L")
    return image

def get_filename(path):
    import ntpath
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

if __name__ == "__main__":
    main()
